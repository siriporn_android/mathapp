package buu.isiriporn.mathapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlin.random.Random

class MultipleActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multiple)
        timer()
        Math()
    }

    var correctScore: Int = 0
    var incorrectScore: Int = 0
    var numberOfClauses:Int = -1

    fun timer () {
        val timer = findViewById<TextView>(R.id.txtTimer)

        object : CountDownTimer(31000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timer.setText("Time   :  " + millisUntilFinished / 1000)
            }
            override fun onFinish() {
                timer.setText("Time's OUT !")
                Toast.makeText(this@MultipleActivity, "Game is over" , Toast.LENGTH_LONG).show()
                val intent = Intent(this@MultipleActivity, ScoreActivity:: class.java)
                intent.putExtra("sum", numberOfClauses.toString())
                intent.putExtra("correct", correctScore.toString())
                intent.putExtra("incorrect", incorrectScore.toString())
                startActivity(intent)
            }
        }.start()
    }

    fun Math() {
        numberOfClauses++
        val firstNum = findViewById<TextView>(R.id.txtNumber1)
        val firstNumRand = Random.nextInt(0, 12)
        firstNum.text = firstNumRand.toString()

        val secondNum = findViewById<TextView>(R.id.txtNumber2)
        val secondNumRand = Random.nextInt(0, 12)
        secondNum.text = secondNumRand.toString()

        val sum = findViewById<TextView>(R.id.txtSum)
        sum.text = numberOfClauses.toString()

        val answer: Int = firstNumRand * secondNumRand

        val position = Random.nextInt(0, 3)
        val firstChoice = findViewById<Button>(R.id.btnFirstChoice)
        val secondChoice = findViewById<Button>(R.id.btnSecondChoice)
        val thirdChoice = findViewById<Button>(R.id.btnThirdChoice)

        if (position == 1) {
            firstChoice.text = (answer + 10).toString()
            secondChoice.text = (answer - 1).toString()
            thirdChoice.text = answer.toString()
        } else if (position == 2) {
            firstChoice.text = (answer - 1).toString()
            secondChoice.text = answer.toString()
            thirdChoice.text = (answer + 10).toString()
        } else {
            firstChoice.text = answer.toString()
            secondChoice.text = (answer - 1).toString()
            thirdChoice.text = (answer + 10).toString()
        }

        val scoreCorrect = findViewById<TextView>(R.id.txtpointRight)
        scoreCorrect.text = correctScore.toString()
        val scoreIncorrect = findViewById<TextView>(R.id.txtpointWrong)
        scoreIncorrect.text = incorrectScore.toString()

        val showText = findViewById<TextView>(R.id.txtShowCorrectText)

        fun addCorrect(score: Int) {
            correctScore ++
            scoreCorrect.text = correctScore.toString()
            showText.text = " PING PONG! Alright!! "
        }

        fun addIncorrect(score: Int) {
            incorrectScore ++
            scoreIncorrect.text = incorrectScore.toString()
            showText.text = " BHU BHUU! It's wrong!!"
        }

        firstChoice.setOnClickListener {
            if (firstChoice.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Math()
            } else {
                addIncorrect(incorrectScore)
                Math()
            }
        }

        secondChoice.setOnClickListener {
            if (secondChoice.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Math()
            } else {
                addIncorrect(incorrectScore)
                Math()
            }
        }

        thirdChoice.setOnClickListener {
            if (thirdChoice.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Math()
            } else {
                addIncorrect(incorrectScore)
                Math()
            }
        }
    }
}