package buu.isiriporn.mathapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_score.*

class ScoreActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score)
        showSum()
        showCorrect()
        showInCorrect()
        Back()
    }

    fun showSum() {
        val sum:String = intent.getStringExtra("sum")?:"0"
        val txtSum = findViewById<TextView>(R.id.txtSum)
        txtSum.text = sum
    }

    fun showCorrect() {
        val correct:String = intent.getStringExtra("correct")?:"0"
        val txtCorrect = findViewById<TextView>(R.id.txtCorrect)
        txtCorrect.text = correct
    }

    fun showInCorrect() {
        val incorrect:String = intent.getStringExtra("incorrect")?:"0"
        val txtIncorrect = findViewById<TextView>(R.id.txtIncorrect)
        txtIncorrect.text = incorrect
    }

    fun Back() {
        btnBack.setOnClickListener {
            Toast.makeText(this@ScoreActivity, "Addition Game" , Toast.LENGTH_LONG).show()
            val intent = Intent(this@ScoreActivity, MainActivity:: class.java)
            startActivity(intent)
        }
    }
}