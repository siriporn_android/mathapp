package buu.isiriporn.mathapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AdditionActivity()
        MinusActivity()
        MultipleActivity()
//        DivideActivity()
    }

    fun AdditionActivity() {
        btnAdd.setOnClickListener {
            Toast.makeText(this@MainActivity, "Addition Game" , Toast.LENGTH_LONG).show()
            val intent = Intent(this@MainActivity, AdditionActivity:: class.java)
            startActivity(intent)
        }
    }

    fun MinusActivity() {
        btnMinus.setOnClickListener {
            Toast.makeText(this@MainActivity, "Minus Game" , Toast.LENGTH_LONG).show()
            val intent = Intent(this@MainActivity, MinusActivity:: class.java)
            startActivity(intent)
        }
    }

    fun MultipleActivity() {
        btnMultiple.setOnClickListener {
            Toast.makeText(this@MainActivity, "Multiple Game" , Toast.LENGTH_LONG).show()
            val intent = Intent(this@MainActivity, MultipleActivity:: class.java)
            startActivity(intent)
        }
    }

//    fun DivideActivity() {
//        btnDivide.setOnClickListener {
//            Toast.makeText(this@MainActivity, "Divide Game" , Toast.LENGTH_LONG).show()
//            val intent = Intent(this@MainActivity, DivideActivity:: class.java)
//            startActivity(intent)
//        }
//    }
}